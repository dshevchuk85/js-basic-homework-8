let paragraphs = document.querySelectorAll('p');
for (let paragraph of paragraphs) {
  paragraph.style.backgroundColor = '#ff0000';
}

let optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentNode);
for(let child of optionsList.childNodes) {
  console.log(child);
  console.log(child.nodeType);
}

let testparagraph = document.getElementById('testParagraph');
testparagraph.innerHTML = 'This is a paragraph';

let mainheadernesting = document.querySelector('.main-header').children;
console.log(mainheadernesting);
for(let element of mainheadernesting) {
  element.classList.add("nav-item");
}

let sectiontitles = document.querySelectorAll('.section-title');
for(let element of sectiontitles) {
  element.classList.remove("section-title");
}
console.log(sectiontitles);
